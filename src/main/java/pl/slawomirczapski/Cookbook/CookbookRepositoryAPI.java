package pl.slawomirczapski.Cookbook;

import java.util.List;


public interface CookbookRepositoryAPI {

    Recipe addRecipe(Recipe recipe);

    Recipe findRecipeById(Integer id);

    List<Recipe> findAllRecipes(String name, String ingredient, Float prepTime);

    Recipe deleteRecipe(Integer id);

    Recipe updateRecipe(Integer id, Recipe recipe);

}
