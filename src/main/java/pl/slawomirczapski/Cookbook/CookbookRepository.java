package pl.slawomirczapski.Cookbook;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CookbookRepository extends JpaRepository<Recipe, Integer> {
}
