package pl.slawomirczapski.Cookbook;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class LocalCookbookRepositoryAPI implements CookbookRepositoryAPI {

    private final List<Recipe> recipes = new ArrayList<>();

    @Override
    public Recipe addRecipe(Recipe recipe) {
        recipes.add(recipe);
        return recipe;
    }

    @Override
    public Recipe findRecipeById(Integer id) {
        return recipes.stream()
                .filter(recipe -> recipe.getId().equals(id))
                .findAny()
                .orElseThrow(() -> new NoSuchElementException(String.valueOf(id)));
    }

    @Override
    public List<Recipe> findAllRecipes(String name, String ingredient, Float prepTime) {
        if (name == null && ingredient == null && prepTime != null) {
            return recipes.stream()
                    .filter(recipe -> recipe.getPrepTime().equals(prepTime))
                    .collect(Collectors.toList());
        }
        if (name != null && ingredient == null && prepTime == null) {
            return recipes.stream()
                    .filter(recipe -> recipe.getName().equals(name))
                    .collect(Collectors.toList());
        }
        if (name == null && ingredient != null && prepTime == null) {
            return recipes.stream()
                    .filter(recipe -> recipe.getIngredients().contains(ingredient))
                    .collect(Collectors.toList());
        }
        return recipes;
    }


    @Override
    public Recipe deleteRecipe(Integer id) {
        Recipe recipeToDelete = recipes.stream()
                .filter(recipe -> recipe.getId().equals(id))
                .findAny()
                .orElseThrow(() -> new NoSuchElementException(String.valueOf(id)));
        recipes.remove(recipeToDelete);
        return recipeToDelete;
    }

    @Override
    public Recipe updateRecipe(Integer id, Recipe recipe) {
        Recipe recipeToUpdate = recipes.stream()
                .filter(r -> r.getId().equals(id))
                .findAny()
                .orElseThrow(() -> new NoSuchElementException(String.valueOf(id)));
        if (recipe.getName() != null) {
            recipeToUpdate.setName(recipe.getName());
        }
        if (recipe.getDescription() != null) {
            recipeToUpdate.setDescription(recipe.getDescription());
        }
        if (recipe.getCalories() != null) {
            recipeToUpdate.setCalories(recipe.getCalories());
        }
        if (recipe.getIngredients() != null) {
            recipeToUpdate.setIngredients(recipe.getIngredients());
        }
        if (recipe.getPrepTime() != null) {
            recipeToUpdate.setPrepTime(recipe.getPrepTime());
        }
        if (recipe.getServings() != null) {
            recipeToUpdate.setServings(recipe.getServings());
        }
        return recipeToUpdate;
    }
}
