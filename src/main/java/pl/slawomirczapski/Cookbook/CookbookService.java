package pl.slawomirczapski.Cookbook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class CookbookService {

    private final CookbookRepository cookbookRepository;

    @Autowired
    public CookbookService(CookbookRepository cookbookRepository) {
        this.cookbookRepository = cookbookRepository;
    }

    public Recipe addRecipe(Recipe recipe) {
        return cookbookRepository.save(recipe);
    }

    public Recipe getRecipeById(Integer id) {
        return cookbookRepository.findById(id).orElseThrow(() -> new NoSuchElementException(String.valueOf(id)));
    }

    public List<Recipe> getAllRecipes() {
        return cookbookRepository.findAll();
    }

    public List<Recipe> getAllRecipesByParam(Integer id, String name, String description, Float calories, List<String> ingredients, Float prepTime, Integer servings) {
        ExampleMatcher matcher = ExampleMatcher.matchingAll().withIgnoreNullValues();
        Example<Recipe> recipeExample = Example.of(
                new Recipe(id, name, description, calories, ingredients, prepTime, servings),
                matcher);
        return cookbookRepository.findAll(recipeExample);
    }

    public Recipe deleteRecipe(Integer id) {
        Recipe recipeToDelete = cookbookRepository.findById(id).orElseThrow(() -> new NoSuchElementException(String.valueOf(id)));
        cookbookRepository.deleteById(id);
        return recipeToDelete;
    }

    public Recipe updateRecipe(Integer id, Recipe recipe) {
        Recipe recipeToUpdate = cookbookRepository.findById(id).orElseThrow(() -> new NoSuchElementException(String.valueOf(id)));
        if (recipe.getName() != null) {
            recipeToUpdate.setName(recipe.getName());
        }
        if (recipe.getDescription() != null) {
            recipeToUpdate.setDescription(recipe.getDescription());
        }
        if (recipe.getCalories() != null) {
            recipeToUpdate.setCalories(recipe.getCalories());
        }
        if (recipe.getIngredients() != null) {
            recipeToUpdate.setIngredients(recipe.getIngredients());
        }
        if (recipe.getPrepTime() != null) {
            recipeToUpdate.setPrepTime(recipe.getPrepTime());
        }
        if (recipe.getServings() != null) {
            recipeToUpdate.setServings(recipe.getServings());
        }
        cookbookRepository.save(recipeToUpdate);
        return recipeToUpdate;
    }

}
