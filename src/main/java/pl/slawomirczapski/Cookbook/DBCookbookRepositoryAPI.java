package pl.slawomirczapski.Cookbook;

import java.util.List;

public class DBCookbookRepositoryAPI implements CookbookRepositoryAPI {
    @Override
    public Recipe addRecipe(Recipe recipe) {
        return null;
    }

    @Override
    public Recipe findRecipeById(Integer id) {
        return null;
    }

    @Override
    public List<Recipe> findAllRecipes(String name, String ingredient, Float prepTime) {
        return null;
    }

    @Override
    public Recipe deleteRecipe(Integer id) {
        return null;
    }

    @Override
    public Recipe updateRecipe(Integer id, Recipe recipe) {
        return null;
    }
}
