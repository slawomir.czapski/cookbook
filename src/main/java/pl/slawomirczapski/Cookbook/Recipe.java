package pl.slawomirczapski.Cookbook;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.*;
import java.util.List;

@Entity
public class Recipe {

    @Id
    @GeneratedValue
    private Integer id;
    @NotEmpty(groups = {AddRecipe.class}, message = "Name is empty or null")
    @Size(groups = {AddRecipe.class, UpdateRecipe.class}, min = 2, max = 40,
            message = "Name outside the required range of characters")
    private String name;
    @Description(groups = {AddRecipe.class, UpdateRecipe.class})
    private String description;
    @Max(groups = {AddRecipe.class, UpdateRecipe.class}, value = 20000)
    private Float calories;
    @ElementCollection
    @NotNull(groups = {AddRecipe.class, UpdateRecipe.class})
    private List<String> ingredients;
    @Min(groups = {AddRecipe.class, UpdateRecipe.class}, value = 15)
    @Max(groups = {AddRecipe.class, UpdateRecipe.class}, value = 240)
    private Float prepTime;
    @Min(groups = {AddRecipe.class, UpdateRecipe.class}, value = 1)
    @Max(groups = {AddRecipe.class, UpdateRecipe.class}, value = 10)
    private Integer servings;

    public Recipe() {
    }

    public Recipe(Integer id, String name, String description, Float calories, List<String> ingredients, Float prepTime, Integer servings) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.calories = calories;
        this.ingredients = ingredients;
        this.prepTime = prepTime;
        this.servings = servings;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getCalories() {
        return calories;
    }

    public void setCalories(Float calories) {
        this.calories = calories;
    }

    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    public Float getPrepTime() {
        return prepTime;
    }

    public void setPrepTime(Float prepTime) {
        this.prepTime = prepTime;
    }

    public Integer getServings() {
        return servings;
    }

    public void setServings(Integer servings) {
        this.servings = servings;
    }

    public interface AddRecipe {
    }

    public interface UpdateRecipe {
    }
}
