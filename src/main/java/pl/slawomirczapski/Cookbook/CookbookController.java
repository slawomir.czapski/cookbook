package pl.slawomirczapski.Cookbook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cookbook")
public class CookbookController {

    private final CookbookService cookbookService;


    @Autowired
    public CookbookController(CookbookService cookbookService) {
        this.cookbookService = cookbookService;

    }

    @PostMapping
    public Recipe addRecipe(@Validated(Recipe.AddRecipe.class) @RequestBody Recipe recipe) {
        return cookbookService.addRecipe(recipe);
    }

    @GetMapping("/{id}")
    public Recipe findRecipeById(@PathVariable Integer id) {
        return cookbookService.getRecipeById(id);
    }

    @GetMapping
    public List<Recipe> findAllRecipes(
            @RequestParam(required = false) Integer id,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String description,
            @RequestParam(required = false) Float calories,
            @RequestParam(required = false) List<String> ingredients,
            @RequestParam(required = false) Float prepTime,
            @RequestParam(required = false) Integer servings)
             {
        return cookbookService.getAllRecipesByParam(id, name, description, calories, ingredients, prepTime, servings);
    }

    @DeleteMapping("/{id}")
    public Recipe deleteRecipeById(@PathVariable Integer id) {
        return cookbookService.deleteRecipe(id);
    }

    @PutMapping("/{id}")
    public Recipe updateRecipeById(@PathVariable Integer id, @Validated(Recipe.UpdateRecipe.class) @RequestBody Recipe recipe) {
        return cookbookService.updateRecipe(id, recipe);
    }

}
