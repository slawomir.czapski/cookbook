package pl.slawomirczapski.Cookbook;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = DescriptionValidator.class)
public @interface Description {
    String message() default "String can be null but can't be blank and be out of range 10-100";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

class DescriptionValidator implements ConstraintValidator<Description, String> {

    @Override
    public void initialize(Description constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return s == null || s.trim().length() > 0 && s.length() >= 10 && s.length() <= 100;
    }
}
