# Cookbook

> Simple CRUD application.

## Table of Contents

* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Setup](#setup)
* [Project Status](#project-status)
* [Room for Improvement](#room-for-improvement)
* [Contact](#contact)

## General Information

The main reason for creating an application is to practice creating a REST API based on the Spring framework.
Project was initiated using spring initializr (https://start.spring.io/) with the Spring WEB module
An application that allows you to:

* adding recipes to the local list
* returning all available recipes
* searching for recipes by parameters
* removal of recipes by id
* updating regulations by id

## Technologies Used

- Java - version 17
- Spring Boot - version 2.7.1
- Swagger - version 3.0.0

## Features

List the ready features here:

- Auto generated API documentation by Swagger
- Adding recipes to the local list
- returning all available recipes
- searching for recipes by parameters
- removal of recipes by id
- updating regulations by id
- returning recipe by id
- own error handling (NuSuchElementException with id in msg)
- Validation:
    * name - not empty, size 2-40
    * description - not blank, can be empty, if not size 10-100
    * calories - max 20000
    * ingredients - not null
    * prepTime - min 15, max 240
    * servings - min 1, max 10
- dependency injection (DI)
- MVC model

## Setup

- Link for Swagger Documentation : http://localhost:8080/swagger-ui/index.html#/

## Project Status

Project is: _in progress_

## Room for Improvement

Room for improvement:

- filtering (Get method) by any parameter (repair ingredient)
- update in service
- tables (list of ingredients in same table ?)

To do:

- Finding by ingredient and list of them
- microservices
- PATCH method
- Security

## Contact

Created by Sławomir Czapski [https://gitlab.com/slawomir.czapski] - feel free to contact me!
Linkedin profile: https://www.linkedin.com/in/slawomir-czapski/
